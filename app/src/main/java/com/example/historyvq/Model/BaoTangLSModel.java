package com.example.historyvq.Model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.historyvq.View.BaoTangLS;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BaoTangLSModel {


    String titleLS;
    String yearLS;
    String imageLS;
    String historyDetail;


    public BaoTangLSModel(String titleLS, String yearLS, String imageLS, String historyDetail) {
        this.titleLS = titleLS;
        this.yearLS = yearLS;
        this.imageLS = imageLS;
        this.historyDetail = historyDetail;
    }



    public String getHistoryDetail() {
        return historyDetail;
    }

    public void setHistoryDetail(String historyDetail) {
        this.historyDetail = historyDetail;
    }



    public BaoTangLSModel() {

    }

    public String getTitleLS() {
        return titleLS;
    }

    public void setTitleLS(String titleLS) {
        this.titleLS = titleLS;
    }

    public String getYearLS() {
        return yearLS;
    }

    public void setYearLS(String yearLS) {
        this.yearLS = yearLS;
    }

    public String getImageLS() {
        return imageLS;
    }

    public void setImageLS(String imageLS) {
        this.imageLS = imageLS;
    }














}

