package com.example.historyvq.Model;

public class NhanVatLSModel {

    String titleNVLS, yearNVLS, historyDetail;

    public String getTitleNVLS() {
        return titleNVLS;
    }

    public void setTitleNVLS(String titleNVLS) {
        this.titleNVLS = titleNVLS;
    }

    public String getYearNVLS() {
        return yearNVLS;
    }

    public void setYearNVLS(String yearNVLS) {
        this.yearNVLS = yearNVLS;
    }

    public String gethistoryDetail() {
        return historyDetail;
    }

    public void setNvlsDetail(String nvlsDetail) {
        this.historyDetail = nvlsDetail;
    }

    public NhanVatLSModel(String titleNVLS, String yearNVLS, String nvlsDetail) {
        this.titleNVLS = titleNVLS;
        this.yearNVLS = yearNVLS;
        this.historyDetail = nvlsDetail;
    }

    public NhanVatLSModel() {

    }

}
