package com.example.historyvq.Model;

public class LichSuDiaPhuongModel {

    String titleLSDP;
    String detailLS;


    public LichSuDiaPhuongModel() {

    }

    public LichSuDiaPhuongModel(String titleLSDP, String detailLS) {
        this.titleLSDP = titleLSDP;
        this.detailLS = detailLS;
    }
    
    public String getDetailLS() {
        return detailLS;
    }

    public void setDetailLS(String detailLS) {
        this.detailLS = detailLS;
    }

    public String getTitleLSDP() {
        return titleLSDP;
    }

    public void setTitleLSDP(String titleLs) {
        this.titleLSDP = titleLs;
    }
}
