package com.example.historyvq.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.historyvq.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPassword extends AppCompatActivity {
    Button btnEmail;
    EditText edtLayLaimk;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        auth = FirebaseAuth.getInstance();

        init();

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               LayLaiMatKhau();
            }
        });

    }

    //Forgot password
    private void LayLaiMatKhau()
    {
        String emailAddress = edtLayLaimk.getText().toString();
        if (!validateForgotPassword())
        {
            return;
        }
        else
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Đăng xử lý...");
            progressDialog.setTitle("Lấy lại mật khẩu");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            auth.sendPasswordResetEmail(emailAddress)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ForgotPassword.this, "Mời bạn kiểm tra email để lấy lại mật khẩu", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                                finish();
                            } else {
                                Toast.makeText(ForgotPassword.this, "Tài khoản không tồn tại", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }
                        }
                    });
        }

    }


    //Check forgotPassword
    private boolean validateForgotPassword()
    {
        String emailAddress = edtLayLaimk.getText().toString();
        if(emailAddress.isEmpty())
        {
            edtLayLaimk.setError("Email không được để trống");
            return false;
        }
        else
        {
            edtLayLaimk.setError(null);
            return true;
        }
    }

    //init view
    private void init()
    {
        btnEmail = findViewById(R.id.btn_email);
        edtLayLaimk = findViewById(R.id.edtlaylaimk);
    }
}
