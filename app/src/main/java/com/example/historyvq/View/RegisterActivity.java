package com.example.historyvq.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.historyvq.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {
    Button btnDangki;
    EditText edtTaiKhoan, edtMatKhau, edtNhapLaiMatKhau;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        init();

        btnDangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DangKy();
            }
        });

    }


    //Đăng kí tài khoản
    private void DangKy()
    {
        String email = edtTaiKhoan.getText().toString();
        String password = edtMatKhau.getText().toString();
        String passwordAgain = edtNhapLaiMatKhau.getText().toString();
        if (!validateRegister())
            return;
        else {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Đăng xử lý...");
            progressDialog.setTitle("Đăng kí");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, "Đăng kí thành công", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                                finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Tài khoản đã tồn tại", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }

                        }
                    });
        }


    }

    //Check resigter
    private boolean validateRegister() {
        String email = edtTaiKhoan.getText().toString();
        String password = edtMatKhau.getText().toString();
        String passwordAgain = edtNhapLaiMatKhau.getText().toString();
        if (email.isEmpty()) {
            edtTaiKhoan.setError("Email không được để trống");
            return false;
        }
        else if(!email.contains("@gmail.com"))
        {
            edtTaiKhoan.setError("Email phải chứa @gmail.com");
            return false;
        } else if (password.isEmpty()) {
            edtMatKhau.setError("Mật khẩu không được để trống");
            return false;
        } else if (passwordAgain.isEmpty()) {
            edtNhapLaiMatKhau.setError("Không được để trống");
            return false;
        } else if (!passwordAgain.equals(password)) {
            edtNhapLaiMatKhau.setError("Không trùng mật khẩu");
            return false;
        } else {
            edtTaiKhoan.setError(null);
            edtMatKhau.setError(null);
            edtNhapLaiMatKhau.setError(null);
            return true;
        }
    }

    //init view
    private void init()
    {
        btnDangki = findViewById(R.id.btn_dangki);
        edtTaiKhoan = findViewById(R.id.edttaikhoan);
        edtMatKhau = findViewById(R.id.edtmatkhau);
        edtNhapLaiMatKhau = findViewById(R.id.edtnhaplaimatkhau);
    }


}
