package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.historyvq.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    CardView baoTangLS, nhanVatLS, keChuyeLS, lichSuDP, tracNghiem, phanHoi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       init();

    }

    private void init(){
        baoTangLS = findViewById(R.id.baotangls);
        nhanVatLS = findViewById(R.id.nhanvatls);
        keChuyeLS = findViewById(R.id.kechuyenls);
        lichSuDP = findViewById(R.id.lichsudp);
        tracNghiem = findViewById(R.id.tracnghiem);
        phanHoi = findViewById(R.id.phanhoi);

        baoTangLS.setOnClickListener(this);
        nhanVatLS.setOnClickListener(this);
        keChuyeLS.setOnClickListener(this);
        lichSuDP.setOnClickListener(this);
        tracNghiem.setOnClickListener(this);
        phanHoi.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.baotangls:
                Intent baotangls = new Intent(MainActivity.this, BaoTangLS.class);
                startActivity(baotangls);
                return;
            case R.id.nhanvatls:
                Intent nhanvatLs = new Intent(MainActivity.this, NhanVatLS.class);
                startActivity(nhanvatLs);
                return;
            case R.id.lichsudp:
                Intent lichsuDp = new Intent(MainActivity.this, LichSuDiaPhuong.class);
                startActivity(lichsuDp);
                return;
            case R.id.kechuyenls:
                Intent kechuyenLS = new Intent(MainActivity.this, KeChuyenLichSu.class);
                startActivity(kechuyenLS);
                return;

        }
    }

}
