package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.historyvq.R;

public class KeChuyenLichSu extends AppCompatActivity {

    ImageView Music , Utube;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ke_chuyen_lich_su);

        Music = (ImageView) findViewById(R.id.Music);
        Music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KeChuyenLichSu.this , Songs.class);
                startActivity(intent);
            }
        });
    }
}
