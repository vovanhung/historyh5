package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.historyvq.R;

public class AccuracyActivity extends AppCompatActivity {
    Button btnXacnhan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accuracy);

        btnXacnhan = findViewById(R.id.btn_accuracy);

        btnXacnhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccuracyActivity.this, NewPassword.class);
                startActivity(intent);
            }
        });
    }
}
