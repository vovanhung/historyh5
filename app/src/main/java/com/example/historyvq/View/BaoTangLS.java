package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.historyvq.Model.BaoTangLSModel;
import com.example.historyvq.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.firebase.ui.database.FirebaseRecyclerAdapter;



public class BaoTangLS extends AppCompatActivity {
    private EditText mSearchField;
    private ImageButton mSearchBtn;

    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bao_tang_ls);

        mUserDatabase = FirebaseDatabase.getInstance().getReference("BaoTangLS");

        Log.d("test", mUserDatabase+"");

        mSearchField =  findViewById(R.id.search_field);
        mSearchBtn =  findViewById(R.id.search_btn);

        mResultList =  findViewById(R.id.result_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

       firebaseUserSearch("T");
        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = mSearchField.getText().toString();

                firebaseUserSearch(searchText);

            }
        });

    }

    private void firebaseUserSearch(String searchText) {

        Query firebaseSearchQuery = mUserDatabase.orderByChild("titleLS").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<BaoTangLSModel, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<BaoTangLSModel, UsersViewHolder>(

                BaoTangLSModel.class,
                R.layout.custom_lisview_baotangls,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, final BaoTangLSModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleLS(), model.getYearLS(), model.getImageLS());
                Log.d("image", model.getImageLS()+"");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(BaoTangLS.this, Detail_Listview.class);
//                        String strName = null;
//                        intent.putExtra("STRING_I_NEED", );

                        Bundle mBundle = new Bundle();
                        mBundle.putString("historyDetail", model.getHistoryDetail());

                        intent.putExtras(mBundle);

                        startActivity(intent);
                    }
                });
            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }


    // View Holder Class

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDetails(Context ctx, String userName, String userStatus, String userImage){

            TextView user_name = mView.findViewById(R.id.name);
            TextView user_status =  mView.findViewById(R.id.profession);
            ImageView user_image =  mView.findViewById(R.id.photo);


            user_name.setText(userName);
            user_status.setText(userStatus);



            Glide.with(ctx).load(userImage).into(user_image);




           Log.d("hinhanh",  Glide.with(ctx).load(userImage).into(user_image)+"");


        }




    }
}
