package com.example.historyvq.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.historyvq.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    Button btnlogin;
    TextView txtDangki, txtQuenmatkhau;
    EditText edtDangNhaptk, edtDangNhapmk;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        init();

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DangNhap();
            }
        });

        txtDangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        txtQuenmatkhau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });
    }


    //Kiểm tra đăng nhập
    private void DangNhap()
    {
        String email = edtDangNhaptk.getText().toString();
        String password = edtDangNhapmk.getText().toString();
        if(!validateLogin())
        {
            return;
        }
        else{
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Đang đăng nhập");
            progressDialog.setTitle("Đăng nhập");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Nếu đăng nhập thành công sẽ chuyển sang giao diện chính
                                progressDialog.dismiss();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(LoginActivity.this, "Tài khoản hoặc mật khẩu sai", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }

                        }
                    });
        }

    }

    //Check login
    private boolean validateLogin()
    {
        String email = edtDangNhaptk.getText().toString();
        String password = edtDangNhapmk.getText().toString();

        if(email.isEmpty())
        {
            edtDangNhaptk.setError("Email không được để trống");
            return false;
        }
        else if(password.isEmpty())
        {
            edtDangNhapmk.setError("Mật khẩu không được để trống");
            return false;
        }
        else{
            edtDangNhaptk.setError(null);
            edtDangNhapmk.setError(null);
            return true;
        }
    }

    private void init()
    {
        btnlogin = findViewById(R.id.btn_login);
        txtDangki = findViewById(R.id.txt_dangki);
        txtQuenmatkhau = findViewById(R.id.txt_quenmatkhau);
        edtDangNhapmk = findViewById(R.id.edtmkdangnhap);
        edtDangNhaptk = findViewById(R.id.edttkdangnhap);
    }


}
