package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.historyvq.Model.BaoTangLSModel;
import com.example.historyvq.Model.NhanVatLSModel;
import com.example.historyvq.R;
import com.example.historyvq.View.Detail_Listview;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.firebase.ui.database.FirebaseRecyclerAdapter;



public class NhanVatLS extends AppCompatActivity {
    private EditText mSearchField;
    private ImageButton mSearchBtn;

    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nhan_vat_ls);

        mUserDatabase = FirebaseDatabase.getInstance().getReference("NhanVatLS");

        Log.d("test", mUserDatabase+"");

        mSearchField =  findViewById(R.id.search_field1);
        mSearchBtn =  findViewById(R.id.search_btn1);

        mResultList =  findViewById(R.id.result_list1);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        lisView();

        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = mSearchField.getText().toString();

                firebaseUserSearch(searchText);

            }
        });

    }

    private void firebaseUserSearch(String searchText) {



        Query firebaseSearchQuery = mUserDatabase.orderByChild("titleNVLS").startAt(searchText).endAt(searchText + "\uf8ff");
        Log.d("test", firebaseSearchQuery+"");

        FirebaseRecyclerAdapter<NhanVatLSModel, NhanVatLS.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<NhanVatLSModel, NhanVatLS.UsersViewHolder>(

                NhanVatLSModel.class,
                R.layout.custom_listview_lichsudiaphuong,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(NhanVatLS.UsersViewHolder viewHolder, final NhanVatLSModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleNVLS(), model.getYearNVLS(), model.gethistoryDetail());

                Log.d("kiemtra",  model.gethistoryDetail()+" ");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(NhanVatLS.this, Detail_Listview.class);
//                        String strName = null;
//                        intent.putExtra("STRING_I_NEED", );

                        Bundle mBundle = new Bundle();
                        mBundle.putString("historyDetail", model.gethistoryDetail());

                        intent.putExtras(mBundle);

                        startActivity(intent);
                    }
                });

            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }

    private void lisView() {



        Query firebaseSearchQuery = mUserDatabase;
        Log.d("test", firebaseSearchQuery+"");

        FirebaseRecyclerAdapter<NhanVatLSModel, NhanVatLS.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<NhanVatLSModel, NhanVatLS.UsersViewHolder>(

                NhanVatLSModel.class,
                R.layout.custom_listview_lichsudiaphuong,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(NhanVatLS.UsersViewHolder viewHolder, final NhanVatLSModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleNVLS(), model.getYearNVLS(), model.gethistoryDetail());

                Log.d("kiemtra",  model.gethistoryDetail()+" ");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(NhanVatLS.this, Detail_Listview.class);
//                        String strName = null;
//                        intent.putExtra("STRING_I_NEED", );

                        Bundle mBundle = new Bundle();
                        mBundle.putString("historyDetail", model.gethistoryDetail());

                        intent.putExtras(mBundle);

                        startActivity(intent);
                    }
                });

            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }



    // View Holder Class

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDetails(Context ctx, String userName, String userStatus, String userImage) {

            TextView name_detail = mView.findViewById(R.id.name);



            name_detail.setText(userName);




        }
    }
}
