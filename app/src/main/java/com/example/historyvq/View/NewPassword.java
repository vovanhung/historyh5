package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.historyvq.R;

public class NewPassword extends AppCompatActivity {
    Button btnNewPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        btnNewPassword = findViewById(R.id.btn_newpassword);

        btnNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewPassword.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
