package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.historyvq.Model.BaoTangLSModel;
import com.example.historyvq.Model.LichSuDiaPhuongModel;
import com.example.historyvq.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class LichSuDiaPhuong extends AppCompatActivity {
    private EditText mSearchField;
    private ImageButton mSearchBtn;

    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lich_su_dia_phuong);
        mUserDatabase = FirebaseDatabase.getInstance().getReference("LichSuDiaPhuong");

        Log.d("test", mUserDatabase+"");

        mSearchField =  findViewById(R.id.search_field);
        mSearchBtn =  findViewById(R.id.search_btn);

        mResultList =  findViewById(R.id.result_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        listView();

        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = mSearchField.getText().toString();

                firebaseUserSearch(searchText);

            }
        });

    }

    private void firebaseUserSearch(String searchText) {

        Toast.makeText(LichSuDiaPhuong.this, "Started Search", Toast.LENGTH_LONG).show();

        Query firebaseSearchQuery = mUserDatabase.orderByChild("titleLSDP").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<LichSuDiaPhuongModel, LichSuDiaPhuong.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<LichSuDiaPhuongModel, LichSuDiaPhuong.UsersViewHolder>(

                LichSuDiaPhuongModel.class,
                R.layout.custom_listview_lichsudiaphuong,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(LichSuDiaPhuong.UsersViewHolder viewHolder, final LichSuDiaPhuongModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleLSDP(), model.getDetailLS());
                Log.d("image", model.getTitleLSDP()+"");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(LichSuDiaPhuong.this, Detail_Listview.class);
//                        String strName = null;
//                        intent.putExtra("STRING_I_NEED", );

                        Bundle mBundle = new Bundle();
                        mBundle.putString("historyDetail", model.getDetailLS());

                        intent.putExtras(mBundle);

                        startActivity(intent);
                    }
                });
            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }

    private void listView() {

        Query firebaseSearchQuery = mUserDatabase;

        FirebaseRecyclerAdapter<LichSuDiaPhuongModel, LichSuDiaPhuong.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<LichSuDiaPhuongModel, LichSuDiaPhuong.UsersViewHolder>(

                LichSuDiaPhuongModel.class,
                R.layout.custom_listview_lichsudiaphuong,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(LichSuDiaPhuong.UsersViewHolder viewHolder, final LichSuDiaPhuongModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleLSDP(), model.getDetailLS());
                Log.d("image", model.getTitleLSDP()+"");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(LichSuDiaPhuong.this, Detail_Listview.class);
//                        String strName = null;
//                        intent.putExtra("STRING_I_NEED", );

                        Bundle mBundle = new Bundle();
                        mBundle.putString("historyDetail", model.getDetailLS());

                        intent.putExtras(mBundle);

                        startActivity(intent);
                    }
                });
            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }


    // View Holder Class

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDetails(Context ctx, String userName, String lsdp){

            TextView user_name = mView.findViewById(R.id.name);

            user_name.setText(userName);



        }




    }
}
