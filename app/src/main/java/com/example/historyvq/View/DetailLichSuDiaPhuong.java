package com.example.historyvq.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.historyvq.Model.BaoTangLSModel;
import com.example.historyvq.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

public class DetailLichSuDiaPhuong extends AppCompatActivity {

    private EditText mSearchField;
    private ImageButton mSearchBtn;

    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase;

    private String value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_lisview_version2);

        checkDuLieu();

        mUserDatabase = FirebaseDatabase.getInstance().getReference(value);

        Log.d("test", mUserDatabase+"");

        mSearchField =  findViewById(R.id.search_field);
        mSearchBtn =  findViewById(R.id.search_btn);

        mResultList =  findViewById(R.id.result_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        firebaseUserSearch("ng");
        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = mSearchField.getText().toString();

                firebaseUserSearch(searchText);

            }
        });

    }

    private void checkDuLieu()
    {
        value = getIntent().getExtras().getString("historyDetail");

        Log.d("test", value+"");

    }

    private void firebaseUserSearch(String searchText) {

        Toast.makeText(DetailLichSuDiaPhuong.this, "Started Search", Toast.LENGTH_LONG).show();

        Query firebaseSearchQuery = mUserDatabase.orderByChild("titleLS").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<BaoTangLSModel, DetailLichSuDiaPhuong.UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<BaoTangLSModel, DetailLichSuDiaPhuong.UsersViewHolder>(

                BaoTangLSModel.class,
                R.layout.activity_detail__listview,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            protected void populateViewHolder(DetailLichSuDiaPhuong.UsersViewHolder viewHolder, final BaoTangLSModel model, int position) {


                viewHolder.setDetails(getApplicationContext(), model.getTitleLS(), model.getYearLS(), model.getImageLS());
                Log.d("image1", model.getTitleLS()+"");

            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }


    // View Holder Class

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDetails(Context ctx, String userName, String userStatus, String userImage){

            TextView user_name = mView.findViewById(R.id.namedetail1);
            TextView user_status =  mView.findViewById(R.id.professiondetal1);
            ImageView user_image =  mView.findViewById(R.id.photodetail1);

            Log.d("hinhanh",  userImage+"");

            user_name.setText(userName);
            user_status.setText(userStatus);

            Glide.with(ctx).load(userImage).into(user_image);

            Log.d("hinhanh",  Glide.with(ctx).load(userImage).into(user_image)+"");


        }




    }


}
